package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Parcelable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private SportDbHelper sportDbHelper;
    private  SimpleCursorAdapter adapter;
    private SwipeRefreshLayout swiperefresh;
    private SwipeDetector swipeDetector = new SwipeDetector();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialise
        sportDbHelper = new SportDbHelper(this);
        SQLiteDatabase db = sportDbHelper.getReadableDatabase();
        sportDbHelper.populateOnlyOneTime(db);
        //sportDbHelper.populate();

        final Cursor cursor = sportDbHelper.fetchAllTeams();
        adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2, cursor,
                new String[] {sportDbHelper.COLUMN_TEAM_NAME, sportDbHelper.COLUMN_LEAGUE_NAME},
                new int[] { android.R.id.text1, android.R.id.text2}, 0);

        ListView teamList = (ListView) findViewById(R.id.listTeams);
        swiperefresh = findViewById(R.id.swiperefresh);
        teamList.setAdapter(adapter);
        db.close();

        teamList.setOnTouchListener(swipeDetector);
        teamList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                if(swipeDetector.swipeDetected()) {
                    if((swipeDetector.getAction() == Action.RL) ||(swipeDetector.getAction() == Action.LR)) {
                        Cursor cursorTeam =  (Cursor) parent.getItemAtPosition(position);
                        Team teamToDelete = sportDbHelper.cursorToTeam(cursorTeam);
                        sportDbHelper.deleteTeam(teamToDelete.getId());

                        adapter.changeCursor(sportDbHelper.getReadableDatabase().query(SportDbHelper.TABLE_NAME,
                                null, null, null, null, null, null));

                        //refresh the listView
                        adapter.notifyDataSetChanged();
                        mt("L'équipe " + teamToDelete.getName() + " a été supprimé !");
                    }
                }else{
                    Cursor cursorTeam =  (Cursor) parent.getItemAtPosition(position);

                    Team clickedTeam = sportDbHelper.cursorToTeam(cursorTeam);

                    Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                    intent.putExtra(Team.TAG, (Parcelable) clickedTeam);
                    startActivityForResult(intent,1);
                }

            }
        });

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new UpdateTask().execute();
            }
        });
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(intent,2);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null){
            Team team = data.getParcelableExtra(Team.TAG);
            switch (requestCode){
                case 1 :
                    sportDbHelper.updateTeam(team);
                    mt("L'équipe " + team.getName() + " a été mise à jour !");
                    break;
                case 2 :
                    sportDbHelper.addTeam(team);
                    mt("L'équipe " + team.getName() + " a été ajouté !");
                    break;
            }
            // change the cursor
            adapter.changeCursor(sportDbHelper.getReadableDatabase().query(SportDbHelper.TABLE_NAME,
                    null, null, null, null, null, null));

            //refresh the listView
            adapter.notifyDataSetChanged();
        }

    }

    public void mt(String string){
        Toast.makeText(this,string,Toast.LENGTH_SHORT).show();
    }

    public class UpdateTask extends AsyncTask<TextView, String, List<Team>> {

        @Override
        protected List<Team> doInBackground(TextView... textViews) {
            List<Team> allTeams = sportDbHelper.getAllTeams();

            for (Team team : allTeams) {
                boolean run = true;
                while (run) {
                    URL url = null;
                    HttpURLConnection urlConnection = null;
                    Match lastEvent = new Match();
                    try {
                        // Team URL
                        url = WebServiceUrl.buildSearchTeam(team.getName());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        JSONResponseHandlerTeam jsrTeam = new JSONResponseHandlerTeam(team);
                        try {
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            jsrTeam.readJsonStream(in);
                        } finally {
                            // Si la team n'existe pas je "cancel" l'AsyncTask
                            if (!jsrTeam.check) {
                                break;
                            }
                            urlConnection.disconnect();
                        }
                        // League URL
                        url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        try {
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            JSONResponseHandlerLeague jsrLeague = new JSONResponseHandlerLeague(team);
                            jsrLeague.readJsonStream(in);
                        } finally {
                            urlConnection.disconnect();
                        }

                        // Event URL
                        url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        try {
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            JSONResponseHandlerEvent jsrEvent = new JSONResponseHandlerEvent(lastEvent);
                            jsrEvent.readJsonStream(in);
                            team.setLastEvent(lastEvent);
                        } finally {
                            urlConnection.disconnect();
                            run = false;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return allTeams;
        }

        @Override
        protected void onPostExecute(List<Team> allTeams) {
            super.onPostExecute(allTeams);
            for (Team team : allTeams) {
                sportDbHelper.updateTeam(team);
            }
            adapter.changeCursor(sportDbHelper.getReadableDatabase().query(SportDbHelper.TABLE_NAME,
                    null, null, null, null, null, null));

            //refresh the listView
            adapter.notifyDataSetChanged();
            swiperefresh.setRefreshing(false);
        }

    }

    public static enum Action {
        LR, // Left to Right
        RL, // Right to Left
        None // when no action was detected
    }

    public class SwipeDetector implements View.OnTouchListener {

        private static final String logTag = "SwipeDetector";
        private static final int MIN_DISTANCE = 100;
        private float downX, upX, upY;
        private Action mSwipeDetected = Action.None;

        public boolean swipeDetected() {
            return mSwipeDetected != Action.None;
        }

        public Action getAction() {
            return mSwipeDetected;
        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    mSwipeDetected = Action.None;
                    return false; // allow other events like Click to be processed
                }
                case MotionEvent.ACTION_MOVE: {
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;

                    // horizontal swipe detection
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        // left or right
                        if (deltaX < 0) {
                            mSwipeDetected = Action.LR;
                            return true;
                        }
                        if (deltaX > 0) {
                            mSwipeDetected = Action.RL;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

}
