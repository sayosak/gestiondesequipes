package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerEvent {
    private static final String TAG = JSONResponseHandlerEvent.class.getSimpleName();

    private Match match;


    public JSONResponseHandlerEvent(Match match) {
        this.match = match;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb == 0) {
                    if (name.equals("idEvent")) {
                        match.setId(reader.nextLong());
                    } else if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        if (reader.peek() != JsonToken.NULL) {
                            match.setHomeScore(reader.nextInt());
                        } else {
                            match.setHomeScore(-1);
                            reader.skipValue();
                        }
                    } else if (name.equals("intAwayScore")) {
                        if (reader.peek() != JsonToken.NULL) {
                            match.setAwayScore(reader.nextInt());
                        }else{
                            match.setAwayScore(-1);
                            reader.skipValue();
                        }
                    } else {
                        reader.skipValue();
                    }
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}
