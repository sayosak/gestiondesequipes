package com.example.tp3;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;


public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);
        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateTask().execute();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TeamActivity.this, MainActivity.class);
        intent.putExtra(Team.TAG, (Parcelable) team);
        setResult(RESULT_OK,intent);
        finish();
        //super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());
        if (team.getTeamBadge() != null) {
            new DownloadImageTask()
                    .execute();
        }
    }


    public class UpdateTask extends AsyncTask<TextView, String, Team> {
        AlertDialog.Builder alertDialogBuilder;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogBuilder = new AlertDialog.Builder(TeamActivity.this);
        }

        @Override
        protected Team doInBackground(TextView... textViews) {
            boolean run = true;

            while (run) {
                URL url = null;
                HttpURLConnection urlConnection = null;
                lastEvent = new Match();
                try {
                    // Team URL
                    url = WebServiceUrl.buildSearchTeam(team.getName());
                    urlConnection = (HttpsURLConnection) url.openConnection();
                    JSONResponseHandlerTeam jsrTeam = new JSONResponseHandlerTeam(team);
                    try {
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        jsrTeam.readJsonStream(in);
                    } finally {
                        // Si la team n'existe pas je "cancel" l'AsyncTask
                        if (!jsrTeam.check) {
                            this.cancel(true);
                            if (isCancelled()) {
                                break;
                            }
                        }
                        urlConnection.disconnect();
                    }
                    // League URL
                    url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                    urlConnection = (HttpsURLConnection) url.openConnection();
                    try {
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        JSONResponseHandlerLeague jsrLeague = new JSONResponseHandlerLeague(team);
                        jsrLeague.readJsonStream(in);
                    } finally {
                        urlConnection.disconnect();
                    }

                    // Event URL
                    url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                    urlConnection = (HttpsURLConnection) url.openConnection();
                    try {
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        JSONResponseHandlerEvent jsrEvent = new JSONResponseHandlerEvent(lastEvent);
                        jsrEvent.readJsonStream(in);
                        team.setLastEvent(lastEvent);
                    } finally {
                        urlConnection.disconnect();
                        run = false;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return team;
        }

        @Override
        protected void onPostExecute(Team team) {
            super.onPostExecute(team);
            updateView();
        }

        @Override
        protected void onCancelled() {
            alertDialogBuilder.setTitle("Cette équipe n'existe pas");
            alertDialogBuilder.setMessage("Veuillez la supprimer");
            alertDialogBuilder.create();
            alertDialogBuilder.show();
        }

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... urls) {

            Bitmap IconBadge = null;
            try {
                URL url = new URL(team.getTeamBadge());
                HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    IconBadge = BitmapFactory.decodeStream(in);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return IconBadge;
        }

        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            imageBadge.setImageBitmap(result);
        }
    }

}


